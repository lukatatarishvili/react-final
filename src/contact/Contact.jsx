import { Helmet } from "react-helmet";

import "./contact.css";

export default function Contact() {
  const submitForm = (event) => {
    alert("");
    event.preventDefault();
  };

  return (
    <>
      <Helmet>
        <title>Contact</title>
      </Helmet>
      <div id="contact">
        <div>
          <h1>Contact? I will find you myself if it's neccessary <span className="name"></span></h1>
        
        </div>
        <form action="/" method="post" onSubmit={submitForm}>
          <div>
            <input type="text" name="name" required placeholder="Name" />
          </div>

          <div>
            <button>Submit</button>
          </div>
        </form>
      </div>
    </>
  );
}
