import { useState } from "react";
import { Link } from "react-router-dom";
import "./header.css";

export default function Header() {
  const [isMenuActive, activeMenu] = useState(false);

  return (
    <header>
      <div></div>
      <div className="menu" onClick={() => activeMenu(!isMenuActive)}>
      <div className="menu_icon"></div>
      <div className="menu_icon"></div>
      <div className="menu_icon"></div>
      </div>

      {isMenuActive && (
        <div className="menu_dialog">
          <nav >
            <div className="menu_item">
              <Link to="/" onClick={() => activeMenu(false)}>
                Home
              </Link>
            </div>
            <div className="menu_item"> 
              <Link to="/about" onClick={() => activeMenu(false)}>
                About me
              </Link>
            </div>
            <div className="menu_item">
              <Link to="/portfolio" onClick={() => activeMenu(false)}>
                Portfolio
              </Link>
            </div>
            <div className="menu_item">
              <Link to="/contact" onClick={() => activeMenu(false)}>
                Contact
              </Link>
            </div>
          </nav>
        </div>
      )}
    </header>
  );
}
