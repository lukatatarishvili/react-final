import { Helmet } from "react-helmet";

import "./home.css";

export default function Home() {
  return (
    <>
      <Helmet>
        <title>Home</title>
      </Helmet>
      <div className="home_container">
        <div className="text-center">
          <p>Say My Name...</p>
        </div>
      </div>
    </>
  );
}
